from django.contrib.auth import get_user_model
from rest_framework import serializers

from api.models import Transaction, Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'name', 'price', 'amount', 'transaction']


class ProductTransactionSerializer(serializers.ModelSerializer):
    product = ProductSerializer(read_only=True, required=False)

    class Meta:
        model = Transaction
        fields = ['id', 'user', 'transaction_type', 'date', 'time', 'sum', 'product']


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ['id', 'user', 'transaction_type', 'date', 'time', 'sum']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'email', 'balance']
