from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework.exceptions import NotFound
from rest_framework.generics import get_object_or_404
from rest_framework.exceptions import ValidationError
from api.models import Transaction, Product
from api.serializers import ProductSerializer, TransactionSerializer


# Создается продукт если необходимо, также проверяется,
# поддерживает ли тип транзакции создание покупки
def create_product_if_needed(data: dict, transaction: Transaction):
    if transaction.transaction_type not in ('buy', 'sell'):
        return
    product = create_product(data['product'], transaction)
    return product


# Проверяется тип транзакции на соответствие одному из 4 типов
def check_transaction_types(transaction_type: str):
    AVAILABLE_TRANSACTION_TYPES = (
        'withdrawal', 'buy', 'sell', 'replenishment')
    if transaction_type not in AVAILABLE_TRANSACTION_TYPES:
        raise ValidationError({'transaction_type': 'Такой операции нет'})


# Проверяется возможность активирования транзации
def check_activating_transaction_types(transaction_type: str):
    if transaction_type not in ('withdrawal', 'replenishment'):
        raise ValidationError({
            'transaction_type': 'Операцию применить невозможно'})


# Получение первичного ключа пользователя по его id,
# записанному в виде числа или строки содержащей число.
# Также ключ можно получить имея его электронную почту
# Код выбрасывает ошибку 404, если не пользователь не был найден
def get_user_pk(unique_value: str) -> int:
    if isinstance(unique_value, int) or unique_value.isdigit():
        user = get_object_or_404(get_user_model(), pk=unique_value)
    else:
        user = get_object_or_404(get_user_model(), email=unique_value)
    return user.pk


# Создается продукт, на основе словаря с данными. Тут же
#  приоисходит и изменение значения итоговой суммы у транзации
def create_product(data: dict, transaction: Transaction) -> Product:
    data['transaction'] = transaction.pk
    serializer = ProductSerializer(data=data)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    product = serializer.instance
    transaction.sum = product.amount * product.price
    transaction.save()
    return product


# Создается транзакция вызывая проверку типов, выбрасываю в
# ответ (response) ошибки, при некорректных данных
def create_transaction(data: dict) -> Transaction:
    data = data
    data['user'] = get_user_pk(data.get('user'))
    serializer = TransactionSerializer(data=data)
    serializer.is_valid(raise_exception=True)
    transaction_type = serializer.validated_data['transaction_type']
    check_transaction_types(transaction_type)
    transaction = Transaction.objects.create(**serializer.validated_data)
    return transaction


# Активируется транзация, баланс пользователя изменяется
def activate_transaction(transaction: Transaction):
    t_type = transaction.transaction_type
    check_activating_transaction_types(t_type)
    if t_type == 'replenishment':
        transaction.user.balance += transaction.sum
    elif t_type == 'withdrawal':
        transaction.user.balance -= transaction.sum
    transaction.user.save()
