from django.contrib.auth import get_user_model
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import Transaction
from api.serializers import TransactionSerializer, ProductTransactionSerializer, UserSerializer
from api.services import get_user_pk, create_transaction, create_product_if_needed, activate_transaction


class DisplayUser(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return Response({'user': request.user.username})


class CreateTransactionView(APIView):
    def post(self, request, *args, **kwargs):
        # Создается транзакия, если тип транзации может принять продукт,
        # продукт создается и в ответ заносятся его данные (данные продукта)
        transaction = create_transaction(request.data)
        product = create_product_if_needed(request.data,
                                           transaction)
        answer = TransactionSerializer(transaction).data

        if product is not None:
            answer['product'] = {
                'name': product.name,
                'price': product.price,
                'amount': product.amount
            }
        return Response(answer)


class ListTransactionView(APIView):
    def get(self, request, *args, **kwargs):
        user_pk = get_user_pk(kwargs['user'])
        transactions = Transaction.objects.filter(user__pk=user_pk)
        serializer = ProductTransactionSerializer(transactions, many=True)
        self.update_product_fields(serializer.data)
        return Response(serializer.data)

    def update_product_fields(self, data):
        # Чтобы в ответ не попадали пустые поля например {"product": null},
        # убираются поля продукта ничего не содержащии
        # также убирается внешний ключ к транзакции в словаре продукта
        for transaction_dict in data:
            if 'product' not in transaction_dict:
                return
            product = transaction_dict.get('product')
            if product:
                del product['transaction']
            else:
                del transaction_dict['product']


class ActivateTransactionView(APIView):
    def post(self, request, *args, **kwargs):
        transaction = create_transaction(request.data)
        activate_transaction(transaction)
        return Response({'status': 'ok'})


class ListUserView(APIView):
    def get(self, request, *args, **kwargs):
        users = get_user_model().objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)
