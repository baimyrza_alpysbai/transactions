from django.contrib.auth import get_user_model
from django.db import models


class Transaction(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,
                             related_name='transactions')
    transaction_type = models.CharField(max_length=100)
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now_add=True)
    sum = models.IntegerField(default=0)


class Product(models.Model):
    transaction = models.OneToOneField('Transaction', on_delete=models.CASCADE,
                                       related_name='product')
    name = models.CharField(max_length=100)
    amount = models.IntegerField()
    price = models.IntegerField()
