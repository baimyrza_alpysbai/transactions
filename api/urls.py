from django.urls import path
from rest_framework_simplejwt import views as jwt_views

from api.views import DisplayUser, CreateTransactionView, ListTransactionView, ActivateTransactionView, ListUserView

urlpatterns = [
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('create_transaction/', CreateTransactionView.as_view()),
    path('user/<str:user>/transactions/', ListTransactionView.as_view()),
    path('user/<str:user>/transactions/', ListTransactionView.as_view()),
    path('activate_transaction/', ActivateTransactionView.as_view()),
    path('users/', ListUserView.as_view()),
]
